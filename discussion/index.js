let posts = []
let count = 1

document.querySelector('#form-add-post').addEventListener('submit', event => {
    event.preventDefault()
    posts.push({
        id: count,
        title: document.querySelector('#txt-title').value, 
        synopsis: document.querySelector('#txt-body').value
    })

    count++
    showPost(posts)
})

const showPost = posts => {
    let postEntries = ""
    posts.forEach(post => {
        postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.synopsis}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
    })

    document.querySelector('#div-post-entries').innerHTML = postEntries
}

const editPost = id => {
    const title = document.querySelector(`#post-title-${id}`).innerHTML
    const synopsis = document.querySelector(`#post-body-${id}`).innerHTML

    document.querySelector('#txt-edit-id').value = id
    document.querySelector('#txt-edit-title').value = title
    document.querySelector('#txt-edit-body').value = synopsis
}

document.querySelector('#form-edit-post').addEventListener('submit', event => {
    event.preventDefault()
    const id = document.querySelector('#txt-edit-id').value
    const length = posts.length

    for (let i = 0; i < length; i++) {
        if (posts[i].id.toString() === id) {
            posts[i].title = document.querySelector('#txt-edit-title').value
            posts[i].synopsis = document.querySelector('#txt-edit-body').value
            break
        }
    }

    showPost(posts)
})

// S43 activity: delete a particular post
const deletePost = id => {
    id = Number(id)
    posts = posts.filter(post => post.id !== id)
    showPost(posts)
}